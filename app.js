const express = require('express')
const app = express()
const cors = require('cors')
const http = require('http')
const httpServer = http.createServer(app)
const fs = require('fs')
const https = require('https')
const privateKey  = fs.readFileSync('./server/certs/server.key', 'utf8');
const certificate = fs.readFileSync('./server/certs/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate}
const httpsServer = https.createServer(credentials, app)

const db_check = require('./server/db_check')
const producer = require('./server/producer')
const worker = require('./server/worker')

const logging = require('./server/logging')
const LOGapp = new logging.log("app.js")

app.use(cors());
app.use(express.json());


//---------------------------------------------------------------------------------------------------------------------
//----- ROUTES --------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

//----- MAIN Page -----------------------------

app.get('/', (req,res) => {

    res.send("Cloud Ops starting")

})

//----- Start Job -----------------------------

app.post('/start', (req,res) => {

    //Проверка получаемых данных ----> вынести в отдельную функцию
    if (!req.body) return res.send("ERROR: app.js: /start: You should to make POST request with data.")

    const task = req.body
    worker.tasksRunner.initTask(task, res)

})

//----- Pause Jobs -----------------------------

app.get('/pause', (req,res) => {

    worker.tasksRunner.pause(res)

})

//----- Resume Jobs -----------------------------

app.get('/resume', (req,res) => {

    worker.tasksRunner.resume(res)

})

//----- Stop Job -----------------------------

app.get('/stop/:id', (req,res) => {

    worker.tasksRunner.stop(req.params.id, res)

})

//----- Get Streaming Log (current) ------------

app.post('/get/current/log', (req,res) => {

    //Проверка получаемых данных ----> вынести в отдельную функцию
    if (!req.body) return res.send("ERROR: app.js: /get/log: You should to make POST request with data.")
    if (!req.body.job.fullname) return res.send("ERROR: app.js: /get/log: You should to set 'fullname' parameter.")
    if (!req.body.job.build_id) return res.send("ERROR: app.js: /get/log: You should to set 'build_id' parameter.")

    producer.getCurrentLogs(req.body, res)

})

//----- Get Task Status  -----------------------

app.get('/get/task/status/:id', (req,res) => {

    Promise.all([producer.getTaskStatus(req.params.id),producer.getInProgressTasks()])
        .then( result => {
            res.json({task:result[0], runningTasks: result[1]})
        })
        .catch( result => {
            LOGapp.error(result)
            res.json(result)
        })

})

//----- Get Tasks Status  ----------------------

app.get('/get/tasks', (req,res) => {

    producer.getTasks()
        .then( status => {
            res.json(status)
        })
        .catch( (message, err) => {
            LOGapp.error(`${message},${err}`)
            res.json({message,err})
        })

})

//----- Get Task full Status  ------------------

app.get('/get/running/tasks', (req,res) => { // List of tasks only, which are running yet

    producer.getInProgressTasks()
        .then( status => {
            res.json(status)
        })
        .catch( (message, err) => {
            LOGapp.error(`${message},${err}`)
            res.json({message,err})
        })

})

//----- Get Job Status  ------------------------

app.get('/get/jobs/:id', (req,res) => {

    producer.getJobs(req.params.id)
        .then( status => {
            res.json(status)
        })
        .catch( (message, err) => {
            LOGapp.error(`${message},${err}`)
            res.json({message,err})
        })

})

//---------------------------------------------------------------------------------------------------------------------
//----- END of ROUTES -------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

db_check.dbCheck()
httpServer.listen(8887,'0.0.0.0',function(){
    LOGapp.info("Jenkins tool started: http://0.0.0.0:8887");
});
httpsServer.listen(8888,'0.0.0.0',function(){
    LOGapp.info(`Jenkins tool started: https://0.0.0.0:8888`)
});

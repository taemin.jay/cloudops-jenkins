FROM node:10.9.0

WORKDIR /opt/

RUN rm /bin/sh \
    && ln -s /bin/bash /bin/sh

RUN apt-get update \
    && apt-get install -y --force-yes curl vim

COPY ./ /opt/

CMD node /opt/app.js
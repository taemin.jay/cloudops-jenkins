const JENKINS = require('jenkins')
const axios = require('axios')
const request = require('request')

const logging = require('./logging')
const LOG_jenApi = new logging.log("jenkins_api.js",'',false)

//----- For the getting CRUMB data, skipping auth to Jenkins
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0


class Jenkins {
    constructor(auth, job, task_id) {
        this.state = {
            jenkins: '',
            jenkins_urlJob: '',
            jenkins_urlAuth: '',
            auth: auth,
            job: job || '',
            crumb_header: '',
            crumb: 'crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)',
            timeout_builid: 500,

            queueID: null,
            buildID: null,
            taskID: task_id || null
        }

        this.create_jenkins()

    }


    //----------------------------------------------
    //----- Настройка Jenkins модуля ---------------
    //----------------------------------------------
    create_jenkins() {
        LOG_jenApi.info("create_jenkins: creating Jenkins module.")
        this.state.jenkins = JENKINS({baseUrl: this.prepare_url(), crumbIssuer: false, headers: this.state.crumb_header})
        return this
    }

    //----- Создание URL для работы с джобой в Jenkins
    prepare_url() {
        const URL = this.create_urlAuth()
        LOG_jenApi.debug(`prepare_url: for job name: ${this.state.job.job.fullname}`)
        return this.state.jenkins_urlAuth = URL, this.state.jenkins_urlJob = URL + this.state.job.job.job_path, URL + this.state.job.job.job_path
    }

    //----- Создание базового URL для Jenkins
    create_urlAuth() {
        const auth = this.state.auth.username + ":" + this.state.auth.token + "@"
        const url = this.state.auth.url.split("//")
        LOG_jenApi.debug("create_urlAuth: created")
        return url[0]+'//'+auth+url[1]
    }

    //----------------------------------------------
    //----- API. Запуск джобы ----------------------
    //----------------------------------------------

    //возвращает ID в общей очереди Jenkins

    startJob() {
        console.log(this.state.job)
        LOG_jenApi.info(` startJob: [${this.state.taskID}, ${this.state.job.job.fullname}] starting...`)
        return new Promise((resolve,reject) => {
            return this.state.jenkins.job.build({ name: this.state.job.job.job_name, parameters: this.state.job.job.params }, (err, queueID) => {
                if (err) {
                    LOG_jenApi.error(` startJob: statusCode ${err.statusCode} ${err}`)
                    return reject(err);
                }

                LOG_jenApi.info(` startJob: [${this.state.taskID}, ${this.state.job.job.fullname}] queueID ${queueID}`)

                this.state.queueID = queueID
                return resolve(queueID)
            });
        })
    }

    //----------------------------------------------
    //----- API. Получить ID джобы -----------------
    //----------------------------------------------
    getbuildID() {
        LOG_jenApi.info(` getbuildID: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.queueID}] Job ID is assigning...`)
        return new Promise((resolve, reject) => {
            const interval = setInterval(() => {                           //
                    axios.get(this.state.jenkins_urlAuth + `/queue/item/${this.state.queueID}/api/json`)
                        .then(response => {
                            if (response.data.executable) {
                                this.state.buildID = response.data.executable.number
                                clearInterval(interval)

                                LOG_jenApi.debug(` getbuildID: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.queueID}] ${response.data.executable}`)
                                LOG_jenApi.info(` getbuildID: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.queueID}, ${this.state.buildID}] Job ID is #${this.state.buildID}`)

                                return resolve(response.data.executable)   //it is job id. Before it was QUEUE ID

                            } else if (response.data.cancelled) {

                                LOG_jenApi.error(` getbuildID: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.queueID}] Job ID and URL is not getting.`)

                                return reject('Request is closed Jenkins.')
                            }
                        })
                        .catch(err => {
                            LOG_jenApi.error(` getbuildID: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.queueID}] Job ID is not getting. ${err}`)
                            return reject(err)
                        })
                }, this.state.timeout_builid)
        })
    }

    //----------------------------------------------
    //----- API. Получить инфо о джобе -------------
    //----------------------------------------------
    getBuildInfo(id) {
        if (id) {
            this.state.buildID = id
        }
        return new Promise((resolve, reject) => {

            LOG_jenApi.info(`getBuildInfo: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.buildID}] Build info fetching...`)

            this.state.jenkins.build.get(this.state.job.job.job_name, this.state.buildID, (err, data) => {
                if (err) {
                    LOG_jenApi.error(`getBuildInfo: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.queueID}, ${this.state.buildID}] No info about the build.`)
                    LOG_jenApi.error(`${err}`)
                    return reject(err)
                }

                LOG_jenApi.info(`getBuildInfo: [${this.state.taskID}, ${this.state.job.job.fullname}, ${this.state.queueID}, ${this.state.buildID}] Build info fetched.`)
                return resolve(data)

            })
        })
    }

    //----------------------------------------------
    //----- API. Получение текущих логов джобы -----
    //----------------------------------------------
    getCurrentLogs(res) {
        let log = this.state.jenkins.build.logStream(this.state.job.job.job_name, this.state.job.job.build_id.toString(), 'html'),
            message
        log.on('data', function(text) {
            text.pipe(res)
        });
        log.on('error', function(err) {
            message = "getCurrentLogs: Logs cann't to be fetching."
            LOG_jenApi.error(`${message}. ${err}`);
            res.write(message)
        });
        log.on('end', function() {
            LOG_jenApi.debug("getCurrentLogs: The end of logs.")
            res.end()
        });

        return this
    }

    //----------------------------------------------
    //----- API. Получение размера логов -----------
    //----------------------------------------------
    getLogSize() {
        return new Promise((resolve,reject) => {
            LOG_jenApi.debug('getLogSize: getting log size')
            const url = this.state.jenkins_urlJob + '/job' + this.state.job.job.job_name + '/' + this.state.buildID + '/logText/progressiveText'
            const r = request(url);
            r.on('response', data => {
                const contentLength = data.headers['x-text-size']
                LOG_jenApi.debug(`getLogSize: got log size ${contentLength}`)
                r.abort();
                resolve(contentLength)
            });
            r.on('error', function(err) {
                LOG_jenApi.error(err)
                reject(err)
            })
        })
    }

    //----------------------------------------------
    //----- API. Получение логов джобы -------------
    //----------------------------------------------
    getLog(start) {
        return new Promise((resolve,reject) => {
            LOG_jenApi.debug(`getLog: getting logs for task ${this.state.buildID}.`)
            let message, options
            start ? options = {start, meta:true} : options = {meta:true}
            this.state.jenkins.build.log(this.state.job.job.job_name, this.state.buildID, options, (err, data) => {
                if (err) {
                    message = `getLog\n${err}`
                    LOG_jenApi.error(message)
                    reject(false)
                }
                LOG_jenApi.debug(`getLog: Logs for ${this.state.buildID} fetched.`)
                resolve(data)
            });

        })
    }


    //----- Логирование всех переменных класса
    show_this_variables() {
        LOG_jenApi.debug(JSON.stringify(this.state.queueID))
        LOG_jenApi.debug(JSON.stringify(this.state.buildID))
        LOG_jenApi.debug(JSON.stringify(this.state.job))
        LOG_jenApi.debug(JSON.stringify(this.state.jenkins_urlAuth))
        LOG_jenApi.debug(JSON.stringify(this.state.jenkins_urlJob))
        return this
    }


    //----- Получение crumb заголовка
    get_crumb() {
        return new Promise( (resolve, reject) => {
            LOG_jenApi.debug("get_crumb_function: getting crumb data...")
            axios.get(this.state.jenkins_urlAuth + this.state.crumb)
                .then(response => {

                    //LOGGING
                    LOG_jenApi.debug("get_crumb_function: crumb data has been get.")
                    this.state.crumb_header = {[response.data.split(":")[0]]: response.data.split(":")[1]}
                    resolve(this.state.crumb_header)

                })
                .catch(err => {

                    //LOGGING
                    LOG_jenApi.error(`ERROR: get_crumb_function: crumb data hasn't been get.\n${err.trace}`)
                    this.state.crumb_header = null
                    reject(this.state.crumb_header)

                })
        }), this
    }

}

module.exports = {
    Jenkins
}

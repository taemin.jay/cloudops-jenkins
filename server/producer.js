const config_check = require('./config_check')
const CONFIG = config_check.check()

const db_api = require('./db_api')
const logging = require('./logging')
const LOGproducer = new logging.log("producer.js",'',false)

const jnks = require('./jenkins_api')
const Jenkins = jnks.Jenkins

const WAITING = 300

const generateTaskJSON = (task, status) => {

    const create_date = new Date().toISOString()
    let Task = {
        task_id: null,
        description: task.description || null,
        environment: task.environment || null,
        username: CONFIG.auth.username || null,
        start_date: create_date,
        update_date: create_date,
        all: task.jobs.length || null,
        status: status,
        jobs: []
    }
    task.jobs.forEach( job => {
        const fullname = formate_fullname(job)
        let job_path = fullname.job_path,
            job_name = fullname.job_name,
            job_info = {
                job_id: job.id,
                job: {...job,job_name,job_path},
                build_id: null,
                url_console: null,
                output: null,
                status: status,
                start_date: create_date,
                update_date: create_date
            }

        Task.jobs.push(job_info)
    })

    return Task

}

const updateJobJSON = (job) => {
    console.log(job)
    const create_date = new Date().toISOString()
    let job_info = {
        job_id: job.job_id,
        job: job.job || null,
        build_id: job.build_id || null,
        url_console: job.url_console || null,
        output: job.output || null,
        status: job.status || null,
        start_date: null,
        update_date: create_date
    }
    return job_info
}

const updateTaskJSON = (task, status) => {

    const create_date = new Date().toISOString()
    let Task = {
        task_id: task.task_id,
        description: task.description || null,
        environment: task.environment || null,
        username: CONFIG.auth.username || null,
        start_date: null,
        update_date: create_date,
        all: task.jobs ? task.jobs.length : null,
        status: status,
        jobs: []
    }
    if (task.jobs) { //hot fix for the the updating status of Task if GET STOP request
        task.jobs.forEach(job => {
            let job_info = {
                job_id: job.job_id,
                job: job,
                build_id: null,
                url_console: null,
                output: null,
                status: status,
                start_date: null,
                update_date: create_date
            }

            Task.jobs.push(job_info)
        })
    }

    return Task

}

const outputFullStatusJSON = (task, jobs) => {
    let status  = {
        task_id: task.ID_TASK,
        description: task.DESCRIPTION,
        environment: task.ENV,
        username: task.USERNAME,
        start_date: task.CREATEDATE,
        update_date: task.UPDATEDATE,
        all: task.ALLJOBS,
        status: task.STATUS,
        jobs: []
    }
    jobs.forEach(job => {
        status.jobs.push({
            job_id: job.ID_JOB,
            build_id: job.JENKINS_ID,
            job: JSON.parse(job.JSON),
            url_console: job.URL_CONSOLE,
            output: job.OUTPUT.split(','),
            status: job.STATUS,
            start_date: job.CREATEDATE,
            update_date: job.UPDATEDATE,

        })
    })

    return status
}

const outputTasksJSON = (tasks) => {
    let data = []
    tasks.forEach(task => {
        data.push({
            task_id: task.ID_TASK,
            description: task.DESCRIPTION,
            environment: task.ENV,
            username: task.USERNAME,
            start_date: task.CREATEDATE,
            update_date: task.UPDATEDATE,
            all: task.ALLJOBS,
            status: task.STATUS
        })

    })

    return data
}

const outputJobsJSON = (jobs) => {
    let data = {jobs:[]}
    jobs.forEach( job => {
        data.jobs.push({
            job_id: job.ID_JOB,
            build_id: job.JENKINS_ID,
            job: JSON.parse(job.JSON),
            url_console: job.URL_CONSOLE,
            output: job.OUTPUT.split(','),
            status: job.STATUS,
            start_date: job.CREATEDATE,
            update_date: job.UPDATEDATE
        })
    })

    return data
}

//------------------------------------------
//----- Formate fullname to jenkins url ----

const formate_fullname = (job) => {

    let buff, url = "", job_path, job_name

    buff = job.fullname.split("/")
    buff.forEach( (path,index) => {
        if (index === buff.length - 1) {
            job_path = url
            job_name = "/" + path
            return
        }
        url = url + "/job/" + path
    })

    return {job_path,job_name}
}


//-------------------------------------------
//----- DB add new task ---------------------

const dbAddTask = (task) => new Promise( (resolve, reject) => {
    const db_conn = db_api.dbConnect()
    db_api.dbAddNewTask(db_conn,task)
        .then( task_id => {
            db_api.dbAddNewJob(db_conn, task, task_id)
                .then( () => {
                    LOGproducer.debug('New job has been added.')
                })
            resolve(task_id)
        })
        .catch( err => {
            LOGproducer.error(err)
            reject(false)
        })
})

const dbUpdateTask = (task) => new Promise( (resolve, reject) => {
    db_api.dbUpdateTask(task)
        .then( () => {
            resolve(true)
        })
        .catch( err => {
            LOGproducer.error(err)
            reject(false)
        })
})

//-------------------------------------------
//----- Init starting Task ------------------

const newTask = (task, status, res) => {

    LOGproducer.info(`newTask: Adding a new Task in db...`)

    const json = generateTaskJSON(task, status)
    return new Promise( (resolve, reject) => {
        dbAddTask(json)
            .then( task_id => {
                LOGproducer.info(`newTask: Task ID is ${task_id}`)
                res.status(200).json(task_id)
                resolve({...json, task_id})

            })
            .catch( err => {

                LOGproducer.error(err)
                res.status(200).json(err)
                reject(err)

            })
    })
}

//-------------------------------------------
//------------ update Task STATUS -----------

const updateStatusTaskToDB = (task, status) => new Promise( (resolve, reject) => {

    //LOGGING
    LOGproducer.info(`Updating the task in the db...`)

    const json = updateTaskJSON(task, status)
    setTimeout( () => {
        dbUpdateTask(json)
            .then(() => {

                //LOGGING
                LOGproducer.info(`updateStatusTaskToDB: Task #${task.task_id} has been updated.`)

                resolve(true)

            })
            .catch(err => {
                LOGproducer.error(err)
                reject(false)
            })
    }, WAITING)
})
//-------------------------------------------
//------------ update Job STATUS ------------

const updateStatusJobToDB = (task_id, job) => {

    //LOGGING
    LOGproducer.info(`newTask: Updating the job in the db...`)

    const json = updateJobJSON(job)
    setTimeout( () => {
        db_api.dbUpdateJobStatus(task_id, json)
            .then(() => {

                //LOGGING
                LOGproducer.info(`updateStatusJobToDB: ${json.job.fullname} has been updated.`)

                return true

            })
            .catch(err => {
                LOGproducer.error(err.trace)
                return false
            })
    }, WAITING)
}

//-------------------------------------------
//------------ update Job BuildID -----------

const updateBuildIDJobToDB = (task_id, job) => {

    //LOGGING
    LOGproducer.info(`updateBuildIDJobToDB: Updating the job in the db...`)

    const json = updateJobJSON(job)
    setTimeout( () => {
        db_api.dbUpdateJobBuildID(task_id, json)
            .then(() => {

                //LOGGING
                LOGproducer.info(`updateBuildIDJobToDB: ${json.job.fullname} has been updated.`)

                return true

            })
            .catch(err => {
                LOGproducer.error(err.trace)
                return false
            })
    }, WAITING)
}

//-------------------------------------------
//------------ update Job Url Console -------

const updateUrlConsoleJobToDB = (task_id, job) => {

    //LOGGING
    LOGproducer.info(`updateUrlConsoleJobToDB: Updating the job in the db...`)

    const json = updateJobJSON(job)
    setTimeout( () => {
        db_api.dbUpdateJobUrlConsole(task_id, json)
            .then(() => {

                //LOGGING
                LOGproducer.info(`updateUrlConsoleJobToDB: ${json.job.fullname} has been updated.`)

                return true

            })
            .catch(err => {
                LOGproducer.error(err.trace)
                return false
            })
    }, WAITING)
}

//-------------------------------------------
//------------ update Job Output ------------

const updateOutputJobToDB = (task_id, job) => {

    //LOGGING
    LOGproducer.info(`updateOutputJobToDB: Updating the job in the db...`)

    const json = updateJobJSON(job)
    setTimeout( () => {
        db_api.dbUpdateJobOutput(task_id, json)
            .then(() => {

                //LOGGING
                LOGproducer.info(`updateOutputJobToDB: ${json.job.fullname} has been updated.`)

                return true

            })
            .catch(err => {
                LOGproducer.error(err.trace)
                return false
            })
    }, WAITING)
}

//-------------------------------------------
//------------ update Jobs Status -----------

const updateStatusJobsToDB = (task, status) => new Promise( (resolve, reject) => {

    //LOGGING
    LOGproducer.info(`updateStatusJobsToDB: Updating the jobs status in the db...`)


    setTimeout( () => {
        db_api.dbUpdateStatusJobsToDB(task, status)
            .then(() => {

                //LOGGING
                LOGproducer.info(`updateStatusJobsToDB: Statuses of jobs for #${task.task_id} have been updated to ${status}.`)

                resolve(true)

            })
            .catch(err => {
                LOGproducer.error(err)
                reject(false)
            })
    }, WAITING)

})

//-------------------------------------------
//--- update status of PAUSED FULL Tasks ----

const updateStatusPAUSEDFullTasks = (status) => new Promise((resolve,reject) => {

    LOGproducer.info(`updateStatusPAUSEDFullTasks: Setting ${status} state for all tasks and jobs...`)
    Promise.all([db_api.dbUpdatePAUSEDTasksStatusToDB(status), db_api.dbUpdatePAUSEDJobsStatusToDB(status)])
        .then( result => {
            LOGproducer.info(`updateStatusPAUSEDFullTasks: Tasks statuses have been updated to ${status}.`)
            resolve(true)
        })
        .catch( err => {
            LOGproducer.error(`updateStatusPAUSEDFullTasks: Something went wrong`)
            LOGproducer.error(err)
            reject(false)
        })
})

//-------------------------------------------
//------- update Jobs Status to PAUSE -------

const updateStatusJobsToPAUSED = () => new Promise((resolve,reject) => {
    //LOGGING
    LOGproducer.info(`updateStatusJobsToPAUSED: Setting PAUSED state for all jobs...`)
    db_api.dbUpdateStatusJobsToPAUSED()
        .then(() => {
            //LOGGING
            LOGproducer.info(`updateStatusJobsToPAUSED: Jobs statuses have been updated to PAUSED.`)
            resolve(true)
        })
        .catch(err => {
            LOGproducer.error(err)
            reject(false)
        })
})

//-------------------------------------------
//------ update Tasks Status to PAUSE -------

const updateStatusTasksToPAUSED = () => new Promise((resolve,reject) => {
    //LOGGING
    LOGproducer.info(`updateStatusTasksToPAUSED: Setting PAUSED state for all Tasks...`)
    db_api.dbUpdateStatusTasksToPAUSED()
        .then(() => {

            //LOGGING
            LOGproducer.info(`updateStatusTasksToPAUSED: Tasks statuses have been updated to PAUSED.`)

            resolve(true)

        })
        .catch(err => {
            LOGproducer.error(err)
            reject(false)
        })
})

//-------------------------------------------
//---- Update FULL Task Status to PAUSE -----

const updateStatusTaskFULLToPAUSED = () => new Promise((resolve,reject) => {
    //LOGGING
    LOGproducer.info(`updateStatusTaskFULLToPAUSED: FULL Tasks status updating...`)
    Promise.all([updateStatusTasksToPAUSED(), updateStatusJobsToPAUSED()])
        .then( results => {
            if (results[0]) {
                console.log(results[0])
                resolve(getInProgressTasks())
            }
        })
        .catch( err => {
            LOGproducer.error(`updateStatusTaskFULLToPAUSED: Something went wrong.`)
            LOGproducer.error(err)
            reject(false)
        })

})

//-------------------------------------------
//---- Update FULL Task Status to Stop ------

const updateStatusTaskFULLTo = (task, status) => new Promise((resolve,reject) => {
    //LOGGING
    LOGproducer.info(`updateStatusTaskFULLTo: FULL Tasks status updating to ${status}`)
    Promise.all([updateStatusTaskToDB(task, status), updateStatusJobsToDB(task, status)])
        .then( results => {
            if (results[0]) {
                LOGproducer.info(`updateStatusTaskFULLTo: FULL Tasks status updated to ${status}`)
                resolve(getInProgressTasks())
            }
        })
        .catch( err => {
            LOGproducer.error(`updateStatusTaskFULLTo: Something went wrong.`)
            LOGproducer.error(err)
            reject(err)
        })

})

//-------------------------------------------
//----- Getting Task status -----------------

//`curl -X GET '0.0.0.0:8887/get/task/status/:id'`
const getTaskStatus = (id) => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectTask(id)
        .then( task => {
            //LOGGING
            LOGproducer.info(`getTaskStatus: dbSelectTask: Task info has been fetch`)
            db_api.dbSelectJobs(id)
                .then( jobs => {
                    //LOGGING
                    LOGproducer.info(`getTaskStatus: dbSelectJobs: Jobs info has been fetch`)
                    resolve(outputFullStatusJSON(task[0],jobs))
                })
                .catch( err => {
                    //LOGGING
                    message = "ERROR: producer.js: dbSelectTask: dbSelectJobs:\nSomething went wrong!"
                    LOGproducer.error(`${message}\n${err}`)
                    reject(`${message} ${err}`)
                })
        })
        .catch( err => {
            //LOGGING
            message = "ERROR: producer.js: dbSelectTask: dbSelectJobs:\nSomething went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            reject(message,err)
        })
})

//-------------------------------------------
//----- Getting Tasks status ----------------

//`curl -X GET '0.0.0.0:8887/get/tasks'`
const getTasks = () => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectAllTasks()
        .then( tasks => {
            //LOGGING
            LOGproducer.info(`getTasks: dbSelectAllTasks: Tasks has been fetch`)
            resolve(outputTasksJSON(tasks))

        })
        .catch( err => {
            //LOGGING
            message = "ERROR: getTasks: dbSelectAllTasks: Something went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            reject(`${message} ${err}`)
        })
})

//-------------------------------------------
//----- Getting Jobs status -----------------

//`curl -X GET '0.0.0.0:8887/get/jobs/:id'`
const getJobs = (id) => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectJobs(id)
        .then( jobs => {
            //LOGGING
            LOGproducer.info(`getJobs: dbSelectJobs: Tasks has been fetch`)
            resolve(outputJobsJSON(jobs))

        })
        .catch( err => {
            //LOGGING
            message = "ERROR: getJobs: dbSelectJobs: Something went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            reject(`${message} ${err}`)
        })
})

const getTaskAndJobs = (task) => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectJobs(task.task_id)
        .then( jobs => {
            //LOGGING
            LOGproducer.info(`getTaskAndJobs: dbSelectJobs: Tasks has been fetch`)
            resolve(Object.assign(task,outputJobsJSON(jobs)))

        })
        .catch( err => {
            //LOGGING
            message = "getTaskAndJobs: dbSelectJobs: Something went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            reject(`${message} ${err}`)
        })
})

//-------------------------------------------
//----- Getting Running Tasks. APP ----------

const getInProgressTasks = () => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectTasksInProgress()
        .then( tasks => {

            console.log(tasks)

            if (tasks.length > 0) {
                resolve(outputTasksJSON(tasks))
            } else {
                resolve(false)
            }
        })
        .catch(err => {
            //LOGGING
            message = "producer.getInProgressTasks() : Something went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            return reject(message,err)
        })
})

//-------------------------------------------
//------ Getting Running Tasks FULL ---------

const checkRunningTasks = () => new Promise((resolve, reject) => {
    let data = []
    getRunningTasks()
        .then( runningTasks => {
            if (runningTasks) {
                runningTasks.forEach( task => {
                    data.push(getTaskAndJobs(task))
                })
                return Promise.all(data).then( results => {
                    resolve(results)
                })

            }
            resolve(false)
        })
        .catch( (err) => {
            LOGproducer.error(` producer.checkRunningTasks() : Something went wrong.`)
            reject(err)
        })
})

//-------------------------------------------
//--------- Getting Running Tasks -----------

const getRunningTasks = () => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectTasksRunning()
        .then( tasks => {

            if (tasks.length > 0) {
                resolve(outputTasksJSON(tasks))
            } else {
                resolve(false)
            }
        })
        .catch(err => {
            //LOGGING
            message = "producer.getRunningTasks() : Something went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            return reject(message,err)
        })
})

//-------------------------------------------
//------- Getting Paused Tasks FULL ---------

const getPausedTasks = () => new Promise((resolve, reject) => {
    let data = []
    getPausedTasksDB()
        .then( pausedTasks => {
            if (pausedTasks) {
                pausedTasks.forEach( task => {
                    data.push(getTaskAndJobs(task))
                })
                return Promise.all(data).then( results => {
                    resolve(results)
                })

            }
        })
        .catch( (err) => {
            LOGproducer.error(` producer.getPausedTasks() : Something went wrong.`)
            reject(err)
        })
})

//-------------------------------------------
//----- Getting Paused Tasks FULL -----------

const checkPausedTasks = () => new Promise((resolve, reject) => {
    let data = []
    getPausedTasksDB()
        .then( pausedTasks => {
            if (pausedTasks) {
                pausedTasks.forEach( task => {
                    data.push(getJobs(task.task_id))
                })
                return Promise.all(data).then( results => {
                    resolve(results)
                })

            }
            resolve(false)
        })
        .catch( (err) => {
            LOGproducer.error(` producer.getPausedTasks() : Something went wrong.`)
            reject(err)
        })
})

//-------------------------------------------
//----- Getting Paused Tasks ----------------

const getPausedTasksDB = () => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectTasksPaused()
        .then( tasks => {

            if (tasks.length > 0) {
                resolve(outputTasksJSON(tasks))
            } else {
                resolve(false)
            }
        })
        .catch(err => {
            //LOGGING
            message = "ERROR: producer.js: getPausedTasks: dbSelectTasksPaused: Something went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            return reject(message,err)
        })
})


//-------------------------------------------
//----- Getting Paused Jobs -----------------

const getPausedJobs = (task_id) => new Promise((resolve, reject) => {
    let message
    db_api.dbSelectJobsPaused(task_id)
        .then( jobs => {

            if (jobs.length > 0) {
                resolve(outputJobsJSON(jobs))
            } else {
                resolve("No paused jobs")
            }
        })
        .catch(err => {
            //LOGGING
            message = "ERROR: producer.js: getPausedJobs: dbSelectJobsPaused: Something went wrong!"
            LOGproducer.error(`${message}\n${err}`)
            return reject(message,err)
        })
})

//------------------------------------------
//----- Get all/streamming logs ------------

//`curl -X GET '0.0.0.0:8887/get/current/log' -H "Content-Type:application/json; charset=utf-8" -d '{"job":{"fullname": "yoda-home/AppManage.2/aws_beta","id": 467}}'`
const getCurrentLogs = (job, res) => {
    const fullname = formate_fullname(job.job)
    job.job.job_path = fullname.job_path
    job.job.job_name = fullname.job_name
    const jenkins = new Jenkins(CONFIG.auth,job)
    jenkins.getCurrentLogs(res)
}

module.exports = {
    newTask,
    updateStatusTaskToDB,
    updateStatusJobsToDB,
    updateStatusJobToDB,
    updateBuildIDJobToDB,
    updateOutputJobToDB,
    updateUrlConsoleJobToDB,
    getTaskStatus,
    getTasks,
    getJobs,
    getCurrentLogs,
    checkPausedTasks,
    checkRunningTasks,
    getInProgressTasks,
    updateStatusTaskFULLToPAUSED,
    getPausedTasks,
    updateStatusPAUSEDFullTasks,
    updateStatusTaskFULLTo
}
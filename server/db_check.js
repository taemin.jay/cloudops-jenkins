/*
-> Проверить наличие ДБ
     - если нет, то
        - создать БД
        -> Проверить наличие таблиц
     - в случае успеха - подключиться для дальнейшей проверки
        -> Проверить наличие таблиц
            - если их нет, либо больше или меньше, чем в схеме, то
                - пересоздать таблицы (удаление какждой таблицы, создать таблицы)
                - закрыть соединение
                - прервать дольнейшую проверку
            - в случае успеха
                -> Проверить количество столбцов
                    - если их нет, либо больше или меньше, чем в схеме, то
                        - пересоздать таблицы (удаление какждой таблицы, создать таблицы)
                        - закрыть соединение
                        - прервать дольнейшую проверку
                    - в случае успеха
                        -> Проверить название и тип столбца
                            - если не соответствует, то
                                - пересоздать таблицы (удаление какждой таблицы, создать таблицы)
                                - закрыть соединение
                                - прервать дольнейшую проверку


 */
const db_api = require('./db_api'),

    logging = require('./logging'),
    LOG_db = new logging.log("db_check.js")

    scheme = [
    {
        name: "tasks",
        columns: [
            {
                name:"ID_TASK",
                type: ["integer", "PRIMARY KEY"]
            },
            {
                name: "DESCRIPTION",
                type: ["text"]
            },
            {
                name: "STATUS",
                type: ["text"]
            },
            {
                name: "ALLJOBS",
                type: ["text"]
            },
            {
                name: "ENV",
                type: ["text"]
            },
            {
                name: "USERNAME",
                type: ["text"]
            },
            {
                name: "CREATEDATE",
                type: ["text"]
            },
            {
                name: "UPDATEDATE",
                type: ["text"]
            }
        ]

    },
    {
        name: "jobs",
        columns: [
            {
                name:"ID_TASK",
                type: ["integer"]
            },
            {
                name: "ID_JOB",
                type: ["integer"]
            },
            {
                name: "JENKINS_ID",
                type: ["integer"]
            },
            {
                name: "STATUS",
                type: ["text"]
            },
            {
                name: "JSON",
                type: ["text"]
            },
            {
                name: "URL_CONSOLE",
                type: ["text"]
            },
            {
                name: "OUTPUT",
                type: ["text"]
            },
            {
                name: "CREATEDATE",
                type: ["text"]
            },
            {
                name: "UPDATEDATE",
                type: ["text"]
            }
        ]
    }
]

const tablesCheck = (db_conn) => {
    LOG_db.info("=======> Checking tables <=======")
    getTablesName(db_conn)
        .then( tables => {
            if (!(countDiff(tables, scheme)) || !(checkTableName(tables, scheme))) {
                throw tables;
            }
            columnsCheck(db_conn, tables)
        })
        .catch( reject => {
            LOG_db.debug(reject)
            LOG_db.info("Recreating")
            dropTables(db_conn, reject)
                .then( messages => {
                    messages.forEach(message => {
                        LOG_db.info(message)
                    })
                    createTables(db_conn, scheme)
                })
                .catch( messages => {
                    messages.forEach(message => {
                        LOG_db.error(message)
                    })
                })
        })
}

const columnsCheck = (db_conn, tables) => {
    LOG_db.info("=======> Checking Columns <=======")
    tables.forEach( table => {
        getColumnsTable(db_conn, table)
            .then( columns => {
                if (!checkColumnsProps(db_conn, columns, table.name)) {
                    throw tables;
                }
            })
            .catch( reject => {
                LOG_db.debug(reject)
                LOG_db.info("Recreating")
                dropTables(db_conn, reject)
                    .then( messages => {
                        messages.forEach(message => {
                            LOG_db.info(message)
                        })
                        createTables(db_conn, scheme)
                    })
                    .catch( messages => {
                        messages.forEach(message => {
                            LOG_db.error(message)
                        })
                    })

            })
    })
}

const countDiff = (array1, array2) => {
    LOG_db.debug("=======> CountDIFF")
    let check = false
    const len_arr1 = array1.length,
        len_arr2 = array2.length
    if (len_arr1 === len_arr2) {
        check = true
    }

    LOG_db.debug(`Count corresponds to the scheme: ${check}`)
    return check
}

//----- Проверить на соответствие имен таблиц схеме -----
const checkTableName = (array1, array2) => {
    LOG_db.info("=======> Checking Tables Name")
    let check = false
    array1.forEach( array => {
        LOG_db.info(`Table name: ${array.name}`)
        if (array2.find( x => x.name === array.name)) {
            check = true
        } else {
            check = false
        }
    })

    LOG_db.debug(`Tables corresponds to the scheme ${check}`)
    return check
}


//---- Получить имена всех таблиц базы ----

const getTablesName = (db_conn) => new Promise((resolve, reject) => {
    db_conn.serialize(() => {
        db_conn.all("select name from sqlite_master where type='table'", function (err, table) {
            if (err) {
                reject(err)
            }
            LOG_db.debug('=======> GET Tables Name')
            LOG_db.debug(`Existing tables in the db: ${JSON.stringify(table)}`)
            resolve(table)
        })
    })
})

//---- Проверка соответствия параметров столбцов схеме

const checkColumnsProps = (db_conn, columns, table_name) => {
    let check = true
    const schema = scheme.find( x => x.name===table_name)

    LOG_db.debug(`>>>>>>>> Table name: ${table_name}`)

    if (!countDiff(columns,schema.columns)) {
        check = false
        LOG_db.warn(`Count of columns (c-${columns.length}) is not corresponding to the scheme(c-${schema.columns.length}).`)
        LOG_db.warn(`Check columns status: ${check}`)
        return check
    }

    LOG_db.debug("=======> Compare Name and Type")
    columns.some( column => {
        let found_schema = schema.columns.find( x => x.name === column.name)
        if ( found_schema === undefined || found_schema.type[0] !== column.type) {
            LOG_db.warn(`Column "${column.name}" (type: ${column.type}) of the "${table_name}" in the db is not corresponding to the scheme`)
            check = false
            return check
        }
    })

    LOG_db.debug(`Props corresponds to the scheme: ${check}`)

    LOG_db.info(`Checking colums status: ${check}`)
    return check
}


//---- Удаление таблиц из базы ----

const dropTables = (db_conn, tables) => new Promise((resolve, reject) => {
    LOG_db.warn("=======> DROP the tables")
    let promises = []
    if (tables.length > 0) {
        tables.forEach(table => {
            promises.push(dropTable(db_conn, table))
        })
        Promise.all(promises)
            .then( messages => {
                resolve(messages)
            })
            .catch( messages => {
                reject(messages)
            })
    } else {
        LOG_db.warn("No one table for the dropping.")
        return resolve([])
    }
})

//---- Удаление таблицы из базы ----

const dropTable = (db_conn, table) => new Promise((resolve,reject) => {
    const query = String(`DROP TABLE IF EXISTS ${table.name}`)
    db_conn.run(query, function(err) {
        if (err) return reject(err)
        return resolve(`Table "${table.name}" has been deleted.`)
    });
})

//---- Получение параметров каждого столбца таблицы
const getColumnsTable = (db_conn, table) => new Promise((resolve,reject) => {
    const query = String(`PRAGMA table_info(${table.name})`)
    db_conn.serialize( () => {
        db_conn.all(query, (err, columns) => {
            if (err) {
                reject(err)
            }
            resolve(columns)
        })
    })
})

//---- Создает таблицы в соответствии со схемой в базей ----

const createTables = (db_conn, scheme) => {
    scheme.forEach( schema => createTable(db_conn, schema))
    db_api.dbClose(db_conn)
}

//---- Создает таблицу по схеме, если не существует в базе ----
const createTable = (db_conn, schema) => {
    const query = `CREATE TABLE IF NOT EXISTS ${schema.name} (${formateToQuery(schema)})`
    db_conn.run(query);
    LOG_db.debug(`=======> CREATING table "${schema.name}". Table has been created.`)
}

//---- Приведение формата схемы данных - имени столбца и его типа - к пригодному для отправки запроса ----
const formateToQuery = (table) => {

    let params = [],
        columns = table.columns

    columns.forEach( column => {
        params.push(column.name+" "+column.type.join(" "))
    })

    params = params.join(", ")

    return params
}



const dbCheck = () => {
    let db_connect = db_api.dbConnect()
    tablesCheck(db_connect)

}


module.exports = {
    dbCheck
}


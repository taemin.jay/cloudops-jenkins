const yaml = require('yaml')
const fs = require('fs')
const path = require("path")
const logging = require('./logging')
const LOGconf = new logging.log("config_check.js")

const CUSTOMconfig = path.join(__dirname, '../conf/jenkins-config.yml')
const DEFAULTconfig = path.join(__dirname,'../jenkins-config-default.yml')

const readApiJson = (config_path) => {

    const file = fs.readFileSync(path.resolve(__dirname, config_path), 'utf8')
    const json = yaml.parse(file)
    return json

}

const check = () => {

    let json

    try {

        json = readApiJson(CUSTOMconfig)

        //LOGGING
        LOGconf.info(`Using Custom config ${CUSTOMconfig}`)

        return json

    } catch (error1) {

        try {

            json = readApiJson(DEFAULTconfig)

            //LOGGING
            LOGconf.warn(`Custom config is not set. Using ${DEFAULTconfig}`)

            return json

        } catch (error2) {

            //LOGGING
            LOGconf.error(`Custom and Default configs are not present.
            Please make sure that you have set ${CUSTOMconfig} file as a volume in the docker-compose
            or add this to 'volume/web-tool' folder`)
            LOGconf.debug(error1)
            LOGconf.debug(error2)

            return false
        }
    }
}

module.exports = {
    check
}
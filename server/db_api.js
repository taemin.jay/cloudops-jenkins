const sqlite3 = require('sqlite3').verbose(),
    path = require("path"),
    db_path = path.join(__dirname, '../server/db_history.sqlite'),
    logging = require('./logging'),
    LOG_db = new logging.log("db_api.js",'',true)

const dbConnect = () => {
    let db_conn = new sqlite3.Database(db_path)
    LOG_db.debug('SQLite DB is exist. Connection established')
    return db_conn
}

const dbClose = (db_conn) => {
    db_conn.close((err) => {
        if (err) {
            return false
        }
        LOG_db.debug('Connection closed.')
    });
}

//-------------------------------------------
//----- ADD to DB a new Task ----------------

const dbAddNewTask = (db_conn, data) => new Promise((resolve,reject) => {

    //LOGGING
    LOG_db.debug("db_api.js: db_add_new_Task: Adding a new task...")

    db_conn.run(`INSERT INTO tasks(DESCRIPTION, STATUS, ALLJOBS, ENV, USERNAME, CREATEDATE, UPDATEDATE) VALUES('${data.description}','${data.status}','${data.all}','${data.environment}','${data.username}','${data.start_date}','${data.update_date}')`, function(err) {
        if (err) {

            //LOGGING
            LOG_db.error(`db_api.js: db_add_new_Task:\nSomething went wrong!\n${err.trace}`)

            return reject(err)
        }

        //LOGGING
        LOG_db.debug(`db_api.js: db_add_new_Task: ID has been inserted ${this.lastID}`)

        return resolve (this.lastID)
    });
})

//-------------------------------------------
//----- ADD to DB a new Job -----------------

const dbAddNewJob = (db_conn, task, task_id) => new Promise((resolve,reject) => {

    //LOGGING
    LOG_db.debug("dbAddNewJob: Adding a new job...")

    task.jobs.forEach( job => {
        const json = JSON.stringify(job.job)

        db_conn.run(`INSERT INTO jobs VALUES('${task_id}','${job.job_id}','${job.build_id}','${job.status}','${json}','${job.url_console}','${job.output}','${job.start_date}','${job.update_date}')`, function (err) {
            if (err) {

                //LOGGING
                LOG_db.error(`dbAddNewJob:\nSomething went wrong!\n${err.trace}`)

                return reject(err)
            }

            //LOGGING
            LOG_db.debug(`dbAddNewJob: ${job.job.fullname} has been inserted`)

            return resolve(true)
        })
    })


})

//-------------------------------------------
//----- Update Task -------------------------

const dbUpdateTask = (task) => new Promise((resolve,reject) => {

    //LOGGING
    LOG_db.debug(`[ task-id: ${task.task_id} ] db_api.js: db_add_new_Task: Updating task...`)

    const db_conn = dbConnect(db_path)
    const updateDate = new Date().toISOString()

    db_conn.serialize( () => {
        db_conn.run(`UPDATE tasks SET STATUS='${task.status}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task.task_id}`, (err) => {
            if (err) {

                //LOGGING
                LOG_db.error(`[ task-id: ${task.task_id} ] db_api.js: dbUpdateTask:\n${err.trace}`)

                reject(err)
            }

            //LOGGING
            LOG_db.debug(`[ task-id: ${task.task_id} ] db_api.js: dbUpdateTask: ${task.task_id} has been updated`)

            resolve(true)

        })
    })

    db_conn.close()

})

//-------------------------------------------
//----- Update Job -------------------------

const dbUpdate_Job = (job, task_id) => new Promise((resolve,reject) => {

    //LOGGING
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: db_update_Job: Updating job...`)

    const db_conn = dbConnect(db_path)
    const updateDate = new Date().toISOString()

    db_conn.run(`UPDATE jobs SET JENKINS_ID='${job.build_id}', STATUS='${job.status}', URL_CONSOLE='${job.url_console}', OUTPUT='${job.output}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task_id} AND ID_JOB=${job.job_id}`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: db_update_Job:\nSomething went wrong!\n${err.trace}`)

            return reject(err.trace)
        }

        //LOGGING
        LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: db_update_Job: Job has been updated`)

        return resolve(true)

    })

    db_conn.close()

})

//-------------------------------------------
//----- Update Jobs -------------------------

const dbUpdate_Jobs = (task) => new Promise((resolve) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    task.jobs.forEach( job => {
        //LOGGING
        LOG_db.debug(`[ task-id: ${task.task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: db_update_Job: Updating job...`)

        db_conn.run(`UPDATE jobs SET JENKINS_ID='${job.build_id}', STATUS='${job.status}', URL_CONSOLE='${job.url_console}', OUTPUT='${job.output}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task.task_id} AND ID_JOB=${job.job_id}`, (err) => {
            if (err) {

                //LOGGING
                LOG_db.error(`[ task-id: ${task.task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: db_update_Job:\nSomething went wrong!\n${err.trace}`)

            }

            //LOGGING
            LOG_db.debug(`[ task-id: ${task.task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: db_update_Job: Jobs have been updated`)
            resolve(true)

        })
    })

    db_conn.close()

})

//-------------------------------------------
//----- Update PAUSED Tasks status ----------

const dbUpdatePAUSEDTasksStatusToDB = (status) => new Promise((resolve, reject) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`dbUpdatePAUSEDTasksStatusToDB: Updating tasks statuses...`)

    db_conn.run(`UPDATE tasks SET STATUS='${status}', UPDATEDATE='${updateDate}' WHERE STATUS='PAUSED'`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`dbUpdatePAUSEDTasksStatusToDB:\nSomething went wrong!\n${err}`)
            reject(err)
        }

        //LOGGING
        LOG_db.debug(`dbUpdatePAUSEDTasksStatusToDB: Tasks have been updated to status ${status}`)
        resolve(true)

    })

})

//-------------------------------------------
//----- Update PAUSED Jobs status -----------

const dbUpdatePAUSEDJobsStatusToDB = (status) => new Promise((resolve, reject) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`dbUpdatePAUSEDJobsStatusToDB: Updating tasks statuses...`)

    db_conn.run(`UPDATE jobs SET STATUS='${status}', UPDATEDATE='${updateDate}' WHERE STATUS='PAUSED'`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`dbUpdatePAUSEDJobsStatusToDB:\nSomething went wrong!\n${err}`)
            reject(err)
        }

        //LOGGING
        LOG_db.debug(`dbUpdatePAUSEDJobsStatusToDB: Tasks have been updated to status ${status}`)
        resolve(true)

    })

})

//-------------------------------------------
//----- Update Jobs status ------------------

const dbUpdateStatusJobsToDB = (task, status) => new Promise((resolve, reject) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`[ task-id: ${task.task_id} ]: dbUpdateStatusJobsToDB: Updating status job...`)

    db_conn.run(`UPDATE jobs SET STATUS='${status}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task.task_id} AND (STATUS='PENDING' OR STATUS='PAUSED')`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`[ task-id: ${task.task_id} ]: dbUpdateStatusJobsToDB:\nSomething went wrong!\n${err}`)
            reject(false)
        }

        //LOGGING
        LOG_db.debug(`[ task-id: ${task.task_id} ]: dbUpdateStatusJobsToDB: Jobs have been updated to status ${status}`)


    })

    resolve(true)

})

const dbUpdateJobStatus = (task_id, job) => new Promise((resolve, reject) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: dbUpdateJobstatus: Updating status job...`)
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: dbUpdateJobstatus: ${JSON.stringify(job)}`)

    db_conn.run(`UPDATE jobs SET STATUS='${job.status}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task_id} AND ID_JOB=${job.job_id}`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: dbUpdateJobstatus:\nSomething went wrong!\n${err}`)
            reject(false)

        }

        //LOGGING
        LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: dbUpdateJobstatus: Job has been updated`)

    })

    resolve(true)

})

//-------------------------------------------
//----- Update Job build_id -----------------

const dbUpdateJobBuildID = (task_id, job) => new Promise((resolve, reject) => {
    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobBuildID: Updating status job...`)
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobBuildID: ${JSON.stringify(job)}`)

    db_conn.run(`UPDATE jobs SET JENKINS_ID='${job.build_id}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task_id} AND ID_JOB=${job.job_id}`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobBuildID:\nSomething went wrong!\n${err}`)
            reject(false)

        }

        //LOGGING
        LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobBuildID: Job has been updated`)

    })

    resolve(true)
})

//-------------------------------------------
//----- Update Job build_id -----------------

const dbUpdateJobOutput = (task_id, job) => new Promise((resolve, reject) => {
    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobOutput: Updating status job...`)
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobOutput: ${JSON.stringify(job)}`)

    db_conn.run(`UPDATE jobs SET OUTPUT='${job.output}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task_id} AND ID_JOB=${job.job_id}`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobOutput:\nSomething went wrong!\n${err}`)
            reject(false)

        }

        //LOGGING
        LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobOutput: Job has been updated`)

    })

    resolve(true)
})

//-------------------------------------------
//----- Update Job build_id -----------------

const dbUpdateJobUrlConsole = (task_id, job) => new Promise((resolve, reject) => {
    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobUrlConsole: Updating status job...`)
    LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobUrlConsole: ${JSON.stringify(job)}`)

    db_conn.run(`UPDATE jobs SET URL_CONSOLE='${job.url_console}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task_id} AND ID_JOB=${job.job_id}`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobUrlConsole:\nSomething went wrong!\n${err}`)
            reject(false)

        }

        //LOGGING
        LOG_db.debug(`[ task-id: ${task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] dbUpdateJobUrlConsole: Job has been updated`)

    })

    resolve(true)
})

//-------------------------------------------
//----- Update Jobs Status to PAUSED --------

const dbUpdateStatusJobsToPAUSED = () => new Promise((resolve, reject) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`dbUpdateStatusJobsToPAUSED: updating to PAUSED state...`)

    db_conn.all(`UPDATE jobs SET STATUS='PAUSED', UPDATEDATE='${updateDate}' WHERE STATUS='PENDING' OR STATUS='IN QUEUE'`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`dbUpdateStatusJobsToPAUSED:\nSomething went wrong!\n${err}`)
            reject(false)
        }

        //LOGGING
        LOG_db.debug(`dbUpdateStatusJobsToPAUSED: Jobs have been updated to status PAUSED`)


    })

    resolve(true)

})

//-------------------------------------------
//----- Update Tasks Status to PAUSED -------

const dbUpdateStatusTasksToPAUSED = () => new Promise((resolve, reject) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    //LOGGING
    LOG_db.debug(`dbUpdateStatusTasksToPAUSED: updating to PAUSED state...`)

    db_conn.all(`UPDATE tasks SET STATUS='PAUSED', UPDATEDATE='${updateDate}' WHERE STATUS='PENDING' OR STATUS='IN QUEUE'`, (err) => {
        if (err) {

            //LOGGING
            LOG_db.error(`dbUpdateStatusTasksToPAUSED:\nSomething went wrong!\n${err}`)
            reject(false)
        }

        //LOGGING
        LOG_db.debug(`dbUpdateStatusTasksToPAUSED: Jobs have been updated to status PAUSED`)


    })

    resolve(true)

})


//-------------------------------------------
//----- Update All Tasks Status -------------

const dbUpdate_AllTasks_status = (tasks) => new Promise((resolve,reject) => {

    const db_conn = dbConnect()
    const updateDate = new Date().toISOString()

    tasks.forEach(task => {

        //LOGGING
        LOG_db.debug(`[ task-id: ${task.task_id} ] db_api.js: dbUpdate_AllTasks_status: Updating status Task...`)

        db_conn.run(`UPDATE tasks SET STATUS='${task.status}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task.task_id}`, (err) => {
            if (err) {

                //LOGGING
                LOG_db.error(`[ task-id: ${task.task_id} ] db_api.js: dbUpdate_AllTasks_status:\n${err}`)

                return reject(err)
            }

            //Checking changes
            /*db_conn.all(`SELECT * FROM tasks WHERE ID_TASK=${task.task_id}`, [], (err, rows) => {
                if (err) {
                    LOG_db.error(err)
                }
                LOG_db.debug(JSON.stringify(rows,null,4))

            });*/


            task.jobs.forEach( job => {
                //LOGGING
                LOG_db.debug(`[ task-id: ${task.task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: dbUpdate_AllTasks_status: Updating status Job...`)

                db_conn.run(`UPDATE jobs SET STATUS='${job.status}', UPDATEDATE='${updateDate}' WHERE ID_TASK=${task.task_id} AND ID_JOB=${job.job_id}`, (err) => {
                    if (err) {

                        //LOGGING
                        LOG_db.error(`[ task-id: ${task.task_id} , job-id: ${job.job_id} , build-id: ${job.build_id} ] db_api.js: dbUpdate_AllTasks_status:\nSomething went wrong!\n${err}`)

                        return reject(err)

                    }

                    //LOGGING
                    LOG_db.debug(`[ task-id: ${task.task_id} ] db_api.js: dbUpdate_AllTasks_status: Task has been updated`)

                })

            })
        })
    })

    return resolve(true)

})

//-------------------------------------------
//----- GET info about the task -------------

const dbSelectTask = (id) => new Promise( (resolve, reject) => {
    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug('db_api.js: dbSelectTask: Getting task info...')
    db_conn.serialize(function () {
        db_conn.all(`SELECT * FROM tasks WHERE ID_TASK=${id}`, [], (err, rows) => {
            if (err) {
                return reject(err)
            }
            return resolve(rows)

        });
    })

})

//-------------------------------------------
//----- GET info about the task -------------

const dbSelectJobs = (id) => new Promise( (resolve, reject) => {
    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug(`db_api.js: dbSelectJobs: Getting jobs info...`)
    db_conn.all(`SELECT * FROM jobs WHERE ID_TASK=${id}`, [], (err, rows) => {
        if (err) {
            return reject(err)
        }
        return resolve(rows)

    });

})

//-------------------------------------------
//----- GET all tasks -----------------------

const dbSelectAllTasks = () => new Promise( (resolve, reject) => {
    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug(`dbSelectAllTasks: Getting all tasks...`)
    db_conn.all(`SELECT * FROM tasks`, [], (err, rows) => {
        if (err) {
            return reject(err)
        }
        return resolve(rows)

    });


})

//-------------------------------------------
//----- GET last 3 tasks --------------------

const dbSelectLast3Tasks = () => new Promise( (resolve, reject) => {
    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug(`db_api.js: dbSelectAllTasks: Getting all tasks...`)
    db_conn.all(`SELECT * FROM tasks ORDER BY ID_TASK DESC LIMIT 3`, [], (err, rows) => {
        if (err) {
            return reject(err)
        }
        return resolve(rows)

    });


})

//-------------------------------------------
//----- GET running tasks -------------------

const dbSelectTasksInProgress = () => new Promise( (resolve, reject) => {

    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug("db_api.js: dbSelectJobs: Getting running tasks...")

    db_conn.serialize(function () {
        db_conn.all(`SELECT * FROM tasks WHERE STATUS="IN QUEUE" OR STATUS="STARTING" OR STATUS="PENDING" OR STATUS="RUNNING" OR STATUS="PAUSED"`, [], (err, rows) => {
            if (err) {
                return reject(err)
            }
            return resolve(rows)

        });
    })
})

//-------------------------------------------
//----- GET running tasks -------------------

const dbSelectTasksRunning = () => new Promise( (resolve, reject) => {

    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug("db_api.js: dbSelectJobs: Getting running tasks...")

    db_conn.serialize(function () {
        db_conn.all(`SELECT * FROM tasks WHERE STATUS="IN QUEUE" OR STATUS="STARTING" OR STATUS="PENDING" OR STATUS="RUNNING"`, [], (err, rows) => {
            if (err) {
                return reject(err)
            }
            return resolve(rows)

        });
    })
})

//-------------------------------------------
//----- GET paused tasks --------------------

const dbSelectTasksPaused = () => new Promise( (resolve, reject) => {

    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug("db_api.js: dbSelectTasksPaused: Getting paused tasks...")

    db_conn.serialize(function () {
        db_conn.all(`SELECT * FROM tasks WHERE STATUS="PAUSED"`, [], (err, rows) => {
            if (err) {
                return reject(err)
            }
            return resolve(rows)

        });
    })
})

//-------------------------------------------
//----- GET paused jobs ---------------------

const dbSelectJobsPaused = (task_id) => new Promise( (resolve, reject) => {

    const db_conn = dbConnect()

    //LOGGING
    LOG_db.debug("db_api.js: dbSelectJobsPaused: Getting paused jobs...")

    db_conn.serialize(function () {
        db_conn.all(`SELECT * FROM jobs WHERE ID_TASK=${task_id} AND STATUS="PAUSED"`, [], (err, rows) => {
            if (err) {
                return reject(err)
            }
            return resolve(rows)

        });
    })
})

module.exports = {
    dbConnect,
    dbClose,
    dbAddNewTask,
    dbUpdateTask,
    dbAddNewJob,
    dbUpdate_Job,
    dbUpdate_Jobs,
    dbUpdateJobStatus,
    dbUpdateJobBuildID,
    dbUpdateJobUrlConsole,
    dbUpdateJobOutput,
    dbUpdate_AllTasks_status,
    dbSelectTask,
    dbSelectJobs,
    dbSelectAllTasks,
    dbSelectLast3Tasks,
    dbSelectTasksRunning,
    dbUpdateStatusJobsToDB,
    dbSelectTasksPaused,
    dbSelectJobsPaused,
    dbSelectTasksInProgress,
    dbUpdateStatusJobsToPAUSED,
    dbUpdateStatusTasksToPAUSED,
    dbUpdatePAUSEDJobsStatusToDB,
    dbUpdatePAUSEDTasksStatusToDB
}
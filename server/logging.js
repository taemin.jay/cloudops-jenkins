const Reset = "\x1b[0m",
    Bright = "\x1b[1m",
    Dim = "\x1b[2m",
    Underscore = "\x1b[4m",
    Blink = "\x1b[5m",
    Reverse = "\x1b[7m",
    Hidden = "\x1b[8m",

    FgBlack = "\x1b[30m",
    FgRed = "\x1b[31m",
    FgGreen = "\x1b[32m",
    FgYellow = "\x1b[33m",
    FgBlue = "\x1b[34m",
    FgMagenta = "\x1b[35m",
    FgCyan = "\x1b[36m",
    FgWhite = "\x1b[37m",

    BgBlack = "\x1b[40m",
    BgRed = "\x1b[41m",
    BgGreen = "\x1b[42m",
    BgYellow = "\x1b[43m",
    BgBlue = "\x1b[44m",
    BgMagenta = "\x1b[45m",
    BgCyan = "\x1b[46m",
    BgWhite = "\x1b[47m"

class log {

    constructor(app,task_id,debug) {
        this.app = app
        this.id = task_id
        this.enable_debug = debug || false
    }

    set_id(id) {
        return this.id = id
    }

    app_f() {

        let message = []
        if (!this.app) this.app = ""
        if (!this.id) this.id = ""

        if (!this.app && !this.id) return ""

        if (this.app && this.id) return `[${this.app}, ${this.id}]`

        return `[${this.app}]`

    }

    date_f() {
        const date = new Date(),
            y = date.getFullYear(),
            m = ("0" + date.getMonth()).slice(-2),
            d = ("0" + date.getDay()).slice(-2),
            t = date.toLocaleTimeString([], { hour12: false }),
            ms = date.getMilliseconds()
        return `${y}-${m}-${d} ${t}:${ms}`
    }

    info(message) {
        console.log(`${this.date_f()} INFO  ${this.app_f()} ${message}`);
    }

    warn(message) {
        console.log(`${this.date_f()} ${FgYellow}WARN${Reset}  ${this.app_f()} ${message}`);
    }

    error(message) {
        console.log(`${FgRed}${this.date_f()} ERROR ${this.app_f()}${Reset} ${message}`);
    }

    debug(message) {
        if (this.enable_debug) {
            return console.log(`${this.date_f()} ${FgBlue}DEBUG${Reset} ${this.app_f()} ${message}`);
        }
        return false
    }

    success(message) {
        console.log(`${this.date_f()} ${FgGreen}SUCCESS${Reset} ${this.app_f()} ${FgGreen}${message}${Reset}`)
    }

}

module.exports = {
    log
}




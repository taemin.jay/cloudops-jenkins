/*
TASKS States:
PENDING, RUNNING, SUCCESS, FAILED, WARNING, STOPPED, PAUSED, NON CONSISTENT

JOBS States:
PENDING, STARTING, RUNNING, SUCCESS, FAILURE, ERROR, STOPPED, PAUSED, NOT FOUND, ENOTFOUND, ETIMEDOUT
 */



const config_check = require('./config_check')
const CONFIG = config_check.check()

const logging = require('./logging')
const LOGworker = new logging.log("worker",'',true)

const jnks = require('./jenkins_api')
const Jenkins = jnks.Jenkins

const producer = require('./producer')

class TasksRunner{
    constructor(config) {
        this.state = {
            running_tasks: 0,
            QUEUE_tasks: [],
            auth: config.auth,
            max_run_task: config.props.max_run_task === "default" ? 1 : config.props.max_run_task,                      //Maximum count of the parallelling running jobs by the one shut
            max_run_job: config.props.max_run_job === "default" ? 1 : config.props.max_run_job,                         //Maximum count of the parallelling running tasks by the one shut
            max_attempt_connect: config.props.max_attempt_connect === "default" ? 5 : config.props.max_attempt_connect, //Maximum count of the attemption to the connect to jenkins
            search_string: config.props.search_string === "default" ? ["Service: "] : config.props.search_string,       //Array (example, ["Service","Status" etc]) of the strings want to search in the logs
            last_bytes_logs: config.props.last_bytes_logs === "default" ? 10000 : config.props.last_bytes_logs,         //Size of the last bytes from the log, which will be includes like as console output default=10000
            request_delay: config.props.request_delay === "default" ? 10000 : config.props.request_delay,               //Time interval for the requests to Jenkins
            timeout_attempt_connect: config.props.timeout_attempt_connect === "default" ? 2000 : config.props.timeout_attempt_connect,//Timeout for the attempts requests to Jenkins
            result: [],
        }
        this.runningTaskIDs = {}
        this.PAUSED = false
    }

    showState() {
        LOGworker.debug(`TasksRunner.showState() : this.state: ${JSON.stringify(this.state, null, 4)}`)
        LOGworker.debug(`TasksRunner.showState() : this.runningTaskIDs: ${Object.keys(this.runningTaskIDs)}`)
        LOGworker.debug(`TasksRunner.showState() : this.PAUSED: ${JSON.stringify(this.PAUSED, null, 4)}`)
        return this
    }

    //----------------------------------------------
    //----- INIT the Task --------------------------
    //----------------------------------------------
    initTask(task, res) {
        LOGworker.info(` TasksRunner.initTask(task,res) : NEW Task ${task.task_id} initialization...`)
        this.showState()
        this.checkingPaused(res) && this.newTask(task, res)

    }

    newTask(task,res) {
        return producer.newTask(task, 'PENDING', res)
            .then( task => {
                this.init(task)
            })
            .catch( (err) => {
                LOGworker.error(` TasksRunner.initTask(task,res) : New Task ${task.task_id} initialization failed.`)
                LOGworker.error(err)
                console.log(err)
            })
    }

    init(task) {
        return this.checkLimit(task) && this.run(task)
    }

    run(task) {
        this.increaseRunningTasks()
        this.DBsetStatusTask(task, "RUNNING")
        return this.runningTaskIDs[task.task_id] = new Task(task)
    }
    //----------------------------------------------
    //----------------------------------------------
    //----------------------------------------------

    checkLimit(task) {
        if (this.state.running_tasks === this.state.max_run_task) {
            LOGworker.info(' TasksRunner.checkLimit(task) : Tasks limit reached. Task has been added to queue...')

            this.state.QUEUE_tasks.push(task)
            this.DBsetStatusTask(task, "PENDING")

            LOGworker.debug(` TasksRunner.checkLimit(task) : current QUEUE ${JSON.stringify(this.state.QUEUE_tasks)}`)
            return false
        }
        LOGworker.debug(` TasksRunner.checkLimit(task) : Task limited is not reached yet. this.state.running_tasks: ${this.state.running_tasks}`)
        return true
    }

    checkingPaused(res) {
        if (this.PAUSED) {
            res.json({status: "PAUSED"})
            return false
        }
        return true
    }

    finishing() {

        if (this.state.QUEUE_tasks.length > 0) {
            const task = this.state.QUEUE_tasks[0]
            this.state.QUEUE_tasks.shift()
            LOGworker.info(` TasksRunner.finishing() : INIT Task #${task.task_id} from QUEUE`)
            return this.init(task)
        } else if (this.state.running_tasks > 0) {
            LOGworker.debug(` TasksRunner.finishing() : Current running tasks: ${Object.keys(this.runningTaskIDs)}`)
            return LOGworker.info(" TasksRunner.finishing() : Waiting for the completion of the lasts tasks...")
        } else {
            LOGworker.debug(` TasksRunner.finishing() : Current running tasks: ${Object.keys(this.runningTaskIDs)}`)
            return LOGworker.success(" TasksRunner.finishing() : Tasks have been completed.")
        }
    }

    stop(task_id,res) {
        LOGworker.warn(` [Task: ${task_id}] TasksRunner.stop() : INIT STOP`)
        LOGworker.debug(` [Task: ${task_id}] TasksRunner.stop() : Current running tasks: ${Object.keys(this.runningTaskIDs)}`)
        return this.stopQueueTasks(task_id, res)
    }

    pause(res) {
        if (!this.PAUSED) {
            LOGworker.warn(` [All Tasks] TasksRunner.pause() : INIT PAUSE`)
            LOGworker.debug(` [All Tasks] TasksRunner.pause() : Current running tasks: ${Object.keys(this.runningTaskIDs)}`)
            LOGworker.debug(` [All Tasks] TasksRunner.pause() : Count tasks in queue: ${this.state.QUEUE_tasks.length}`)
            Promise.all([this.setPausedState(),this.pauseRunningTasks()])
                .then( results => {
                    producer.getInProgressTasks()
                        .then(tasks => {
                            res.json({...results[1], tasks})
                        })
                        .catch((message, err) => {
                            res.json({...results[1], message, err})
                        })
                })

        } else {
            res.json({status:"PAUSED"})
        }
        return this
    }

    resume(res) {

        if (this.PAUSED) {

            const runningTasks = this.runningTaskIDs,
                runningTasksIDS = Object.keys(runningTasks)

            if (runningTasksIDS.length > 0) {
                runningTasksIDS.forEach( task => {
                    runningTasks[task].resume()
                })
            }
            producer.updateStatusPAUSEDFullTasks("PENDING")
                .then( () => {
                    producer.checkRunningTasks()
                        .then( tasks => {
                            if (tasks.length > 0) {
                                tasks.forEach(task => {
                                    this.init(task)
                                })
                            }
                            this.PAUSED = false
                            res.json({tasks})
                        })
                })
            /*producer.getPausedTasks()
                .then(tasks => {
                    let buff = []
                    if (tasks.length > 0) {
                        tasks.forEach(task => {
                            buff.push(task.task_id)
                            this.init(task)
                        })
                    }
                    this.PAUSED = false
                    res.json({tasks: buff, status: "RESUMED"})
                })*/
        } else {
            res.json({status: "RESUMED"})
        }
    }

    setPausedState() {
        return new Promise ( resolve => {
            this.PAUSED = true
            this.state.QUEUE_tasks = []
            this.DBsetPausedState()
                .then( runningTasks => {
                    resolve(runningTasks)
                })
        })
    }

    pauseRunningTasks() {
        return new Promise( resolve => {
            const tasks = Object.keys(this.runningTaskIDs)
            let buff = []
            if (tasks.length > 0) {
                tasks.forEach( task => {
                    buff.push({task, message: this.runningTaskIDs[task].pause()})
                })
                resolve({runningTasks: buff, status: "WAITINGLAST"})
            }
            resolve({status: "PAUSED"})
        })
    }

    stopQueueTasks(task_id, res) {
        const task = this.state.QUEUE_tasks.find(x => x.task_id == task_id),
            runningTasks = this.runningTaskIDs

        if (task || this.PAUSED) {
            this.state.QUEUE_tasks.filter( x => x !== task)
            producer.updateStatusTaskFULLTo(task || {task_id}, "STOPPED")
                .then( runningTasks => {
                    return res.json({stoppedTask:task_id, runningTasks})
                })
        } else if (runningTasks[task_id]) {
            this.DBsetStatusJobs({task_id}, "STOPPED")
            return runningTasks[task_id].stop(res)
        } else {
            return res.json({status: "STOPPED"})
        }
    }

    increaseRunningTasks() {
        LOGworker.debug(` TasksRunner.increaseRunningTasks() : Currents running tasks: ${Object.keys(this.runningTaskIDs)}`)
        this.state.running_tasks++
        return this
    }

    decreaseRunningTasks(task) {
        delete this.runningTaskIDs[task.task_id]
        LOGworker.debug(` TasksRunner.decreaseRunningTasks() : Currents running tasks: ${Object.keys(this.runningTaskIDs)}`)
        this.state.running_tasks--
        return this
    }

    DBsetPausedState() {
        return new Promise ( resolve => {
            producer.updateStatusTaskFULLToPAUSED()
                .then( runningTasks => {
                    resolve(runningTasks)
                })
        })
    }

    DBsetState(task, status) {
        this.DBsetStatusTask(task, status)
        this.DBsetStatusJobs(task, status)
    }

    DBsetStatusTask(task, status) {
        producer.updateStatusTaskToDB(task, status)
        return this
    }

    DBsetStatusJobs(task, status) {
        producer.updateStatusJobsToDB(task, status)
        return this
    }

}

class Task {
    constructor(task) {
        this.state = {
            jobs_statuses: [],
            QUEUE_jobs: '',
            running_jobs: 0,
            STOPPED: false,
            PAUSED: false,
        }
        this.task = task
        this.jobsIDs = {}

        this.sortJobs().showState().runTask()

    }

    showState() {
        LOGworker.debug(` Task.showstate() : this.state: ${JSON.stringify(this.state,null,4)}`)
        LOGworker.debug(` Task.showstate() : this.task: ${JSON.stringify(this.task,null,4)}`)
        return this
    }

    runTask() {
        this.checkingSTOP() && this.checkingPAUSE() && this.start()
    }

    start() {
        if (this.state.QUEUE_jobs.length > 0) {
            let queue_jobs = JSON.parse(JSON.stringify(this.state.QUEUE_jobs))
            for (let i = 0; i < queue_jobs.length; i++) {
                const job = queue_jobs[i]
                this.checkLimit(job) && this.runJob(job)
            }
        } else if (this.state.running_jobs > 0) {
            LOGworker.debug(` Task.runTask() : Waiting for the completion of last jobs...`)
        } else {
            this.finished()
        }
    }

    runJob(job) {
        LOGworker.info(` Task.runJobs(job) : [Task: ${this.task.task_id}, Job: ${job.job_id}] RUN JOB`)
        this.increaseRunningJobs()
        this.state.QUEUE_jobs.shift()
        LOGworker.debug(` Task.runJobs(job) : [Task: ${this.task.task_id}]: Current QUEUE of jobs ${JSON.stringify(this.state.QUEUE_jobs)}`)
        return this.jobsIDs[job.job_id] = new Job(this, job)

    }

    finished() {
        LOGworker.debug(` Task.runTask() : QUEUE_jobs is empty.`)
        LOGworker.success(` Task.runTask() : Task #${this.task.task_id} FINISHED`)
        this.DBsetStatusTask(this.analysisTaskStatus())
        return tasksRunner.decreaseRunningTasks(this.task).showState().finishing()
    }

    checkingSTOP() {
        LOGworker.debug(` Task.checkingSTOP() : Checking STOP status...${this.state.STOPPED}`)
        if (this.state.STOPPED) {
            this.state.QUEUE_jobs.forEach( () => {
                this.addJobStatus("STOPPED")
            })
            this.state.QUEUE_jobs = []
            this.finished()
            return false
        }
        return true
    }

    checkingPAUSE() {
        LOGworker.debug(` Task.checkingPAUSE() : Checking PAUSE status...${this.state.PAUSED}`)
        if (this.state.PAUSED) {
            this.state.QUEUE_jobs.forEach( () => {
                this.addJobStatus("PAUSED")
            })
            this.state.QUEUE_jobs = []
            this.finished()
            return false
        }
        return true
    }

    checkLimit(job) {
        LOGworker.debug(` Task.checkLimit(job) : Checking limites...`)
        if (this.state.running_jobs === tasksRunner.state.max_run_job) {
            LOGworker.info(` Task.checkLimit(job) : [Task: ${this.task.task_id}]: Jobs limit reached. ${job.job.fullname} will be started in the next available slot...`)
            return false
        }
        LOGworker.debug(` Task.checkLimit(job) : Jobs limited is not reached yet. this.state.running_tasks: ${this.state.running_jobs}`)
        return true
    }

    stop(res) {
        this.state.STOPPED = true
        return res.json({message: this.messageForSTOPState()})
    }

    pause() {
        this.state.PAUSED = true
        return this.messageForPAUSEState()
    }

    resume() {
        return this.state.PAUSED = false
    }

    sortJobs() {
        const queue_jobs = this.task.jobs.sort((a, b) => {
            if (a.job_id && b.job_id) {
                return a.job_id - b.job_id
            }
            return -1
        })
        return this.state.QUEUE_jobs = JSON.parse(JSON.stringify(queue_jobs)), this
    }

    messageForSTOPState() {

        let data = []
        Object.keys(this.jobsIDs).forEach( id => {
            if (this.jobsIDs[id].state.job.status === "RUNNING" || this.jobsIDs[id].state.job.status === "STARTING") {
                data.push(`Waiting for the completion of the ${this.jobsIDs[id].state.job.job.params.Service}`)
            }
        })

        return data
    }

    messageForPAUSEState() {

        let data = []
        Object.keys(this.jobsIDs).forEach( id => {
            if (this.jobsIDs[id].state.job.status === "RUNNING" || this.jobsIDs[id].state.job.status === "STARTING") {
                data.push(this.jobsIDs[id].state.job.job.params.Service)
            }
        })

        return `Waiting for the completion of the ${data}`
    }

    increaseRunningJobs() {
        this.state.running_jobs++
        LOGworker.debug(` Task.increaseRunningJobs() : Count ${this.state.running_jobs}. Current running jobs: ${Object.keys(this.jobsIDs)}`)
        return this
    }

    decreaseRunningJobs(job) {
        console.log(Object.keys(this.jobsIDs))
        delete this.jobsIDs[job.job_id]
        console.log(Object.keys(this.jobsIDs))
        this.state.running_jobs--
        LOGworker.debug(` Task.decreaseRunningJobs() : Count ${this.state.running_jobs}. Current running jobs: ${Object.keys(this.jobsIDs)}`)
        return this
    }

    showJob(job) {
        return LOGworker.debug(` Task.showJob(job) : [Task: ${this.task.task_id}]: Job ${job.job_name} running_jobs:${this.state.running_jobs}`)
    }

    DBsetStatusTask(status) {
        producer.updateStatusTaskToDB(this.task, status)
        return this
    }

    addJobStatus(status) {
        this.state.jobs_statuses.push(status)
        return this
    }

    analysisTaskStatus() {
        const job_status_error = [
                "NOT FOUND",
                "FAILURE",
                "ERROR",
                "ENOTFOUND",
                "ETIMEDOUT"
            ],
            job_status_success = [
                "SUCCESS"
            ],
            job_status_other = [
                "IN QUEUE",
                "PENDING",
                "RUNNING",
                "STARTING"
            ],
            job_status_stopped = ["STOPPED"],
            job_status_paused = ["PAUSED"]


        console.log(this.state.jobs_statuses)
        if (this.state.jobs_statuses.some( job => job_status_other.includes(job))) {
            LOGworker.error(` Task.analysisTaskStatus() : [Task: ${this.task.task_id}]: The completed task has no consistent statuses of jobs: ${this.state.jobs_statuses} `)
            return "NON CONSISTENT" //если присутствует джобы с не валидным статусом для завершенной таски (т.е. при завершенной таске - джоба не может иметь статусы из job_status_other)
        } else if (this.state.jobs_statuses.some( job => job_status_stopped.includes(job))) {
            return "STOPPED"    //если таска была остановлена
        } else if (this.state.jobs_statuses.some( job => job_status_paused.includes(job))) {
            return "PAUSED"     //если таска была приостановлена
        } else if (this.state.jobs_statuses.some( job => job_status_error.includes(job)) && this.state.jobs_statuses.some( job => job_status_success.includes(job))) {
            return "WARNING"    //если хотя бы одна джоба завершилась не корректно, либо не найдена
        } else if (this.state.jobs_statuses.some( job => job_status_error.includes(job))) {
            return "FAILED"    //если все джобы завершились успешно
        } else {
            return "SUCCESS"    //если ни одна из джоб не завершилась корректно
        }
    }

}


class Job {
    constructor(Task, job) {
        this.state = {
            job: job,
            attempt: 0
        }
        this.jenkins = null
        this.Task = Task

        this.showState().init()

    }

    showState() {
        LOGworker.debug(` Job.showState() : [Task: ${this.Task.task.task_id}]: this.state: ${JSON.stringify(this.state,null,4)}`)
        return this
    }

    init() {
        this.jenkins = new Jenkins(tasksRunner.state.auth, this.state.job, this.Task.task.task_id)
        return this.checkStatusJOB()
    }

    checkStatusJOB() {
        const job_status = [
            "NOT FOUND",
            "FAILURE",
            "ERROR",
            "ENOTFOUND",
            "ETIMEDOUT",
            "STOPPED",
            "SUCCESS"
        ]

        LOGworker.debug(` Job.checkStatusJOB() : [Task: ${this.Task.task.task_id}] Job status checking`)
        if (!job_status.includes(this.state.job.status)) {
            LOGworker.debug(` Job.checkStatusJOB() : [Task: ${this.Task.task.task_id}] Job status ${this.state.job.status}. Checking status in Jenkins`)
            const id = this.state.job.build_id,
                callback2 = this.run()
            this.checkStatusJENKINS({id, callback2})
            return false
        }
        return this.finished()
    }

    run() {
        this.DBsetStatusJob("STARTING")
        LOGworker.debug(` Job.run() : [Task: ${this.Task.task.task_id}, Job: ${this.state.job.job_id}] STARTING ${JSON.stringify(this.state.job,null,4)}`)
        this.jenkins.startJob()
            .then( () => {
                this.DBsetStatusJob("RUNNING")
                this.jenkins.getbuildID()
                    .then( data => {
                        this.jenkins.show_this_variables()
                        this.state.ID = data.number
                        this.DBsetStartedJob({id: data.number, url_console: data.url + 'console'})
                        this.finishing()
                    })
            })
            .catch( (err) => {
                this.runCatch(err)
            })
    }

    runCatch(err) {
        LOGworker.error(` Job.runCatch(err) : Job cann't be started`)
        LOGworker.error(`${err}`)
        let status
        if (err.code === "ETIMEDOUT") {
            if (this.state.attempt < tasksRunner.state.max_attempt_connect) {
                this.state.attempt++
                return setTimeout(() => {
                    this.run()
                }, tasksRunner.state.timeout_attempt_connect)
            } else {
                status = 'ETIMEDOUT'
                this.DBsetStatusJob(status)
                this.DBsetOutputJob([err.toString() || ""])
            }
        } else if (err.notFound || err.code === "ENOTFOUND") {
            status = 'NOT FOUND'
            this.DBsetStatusJob(status)
            this.DBsetOutputJob([err.toString() || ""])
        } else {
            status = 'ERROR'
            this.DBsetStatusJob(status)
            this.DBsetOutputJob([err.toString() || ""])
        }
        this.finished(err, status)
    }

    checkStatusJENKINS({id = false, callback1, callback2}) {
        this.jenkins.getBuildInfo(id)
            .then( (data) => {
                if (data) {
                    if (data.result) {
                        this.state.job.status = data.result
                        this.DBsetFinishedJob(data)
                            .then ( resolve => {
                                this.finished(data.result)
                            })
                        if (callback1) {
                            callback1()
                        }
                    } else if (!callback1) {
                        this.finishing()
                    }
                }
            })
            .catch( () => {
                if (callback1) {
                    callback1()
                    return this.finished()
                } else if (callback2) {
                    return callback2()
                }
            })
    }

    finishing() {
        const interval = setInterval(() => {
            const callback1 = clearInterval(interval)
            this.checkStatusJENKINS({callback1})
        }, tasksRunner.state.request_delay)
    }

    finished(err, status = this.state.job.status) {
        if (err) {
            LOGworker.warn(` [Task: ${this.Task.task.task_id}] Job.finished() : Job ${this.state.job.job.fullname} Cann't to be proccesed in the Jenkins.`)
        } else {
            LOGworker.success(` [Task: ${this.Task.task.task_id}] Job.finished() : Job ${this.state.job.job.fullname} #${this.state.ID} FINISHED`)
        }
        return this.Task.addJobStatus(status).decreaseRunningJobs(this.state.job).runTask()
    }

    DBsetFinishedJob(data) {
        return new Promise( (resolve, reject) => {
            this.getOutputJob()
                .then( output => {
                    this.DBsetOutputJob(output)
                    this.DBsetStatusJob(data.result)
                })
                .catch( err => {
                    reject(err)
                })
                .finally( () => {
                    resolve(true)
                })
        })
    }

    DBsetStatusJob(status) {
        producer.updateStatusJobToDB(this.Task.task.task_id,{...this.state.job, build_id: this.state.ID, status: status})
        return this
    }

    DBsetStartedJob(data) {
        producer.updateBuildIDJobToDB(this.Task.task.task_id,{...this.state.job, build_id: data.id})
        producer.updateUrlConsoleJobToDB(this.Task.task.task_id,{...this.state.job, build_id: data.id, url_console: data.url_console})
        return this
    }

    DBsetOutputJob(output) {
        producer.updateOutputJobToDB(this.Task.task.task_id,{...this.state.job, output: output})
        return this
    }

    getOutputJob() {
        return new Promise((resolve, reject) => {
            this.jenkins.getLogSize()
                .then(full_bytes_logs => {
                    let start
                    if (full_bytes_logs > this.Task.state.last_bytes_logs) start = full_bytes_logs - this.Task.state.last_bytes_logs
                    this.jenkins.getLog(start)
                        .then(logs => {
                            this.logParser(logs.text)
                                .then(output => {
                                    resolve(output)
                                })
                        })
                        .catch(err => {
                            LOGworker.error(` Job.getOutputJob() : Can't to fetch logs. Please, use the console output.`)
                            reject(err)
                        })
                })
                .catch(err => {
                    LOGworker.error(` Job.getOutputJob() : Can't to fetch logs metadata (size of logs). Please, use the console output.`)
                    reject(err)
                })
        })
    }

    logParser(logs) {
        return new Promise((resolve) => {
            LOGworker.info(` Job.logParser(logs) : Parsing log. Preparing output.`)
            let reg, buff, output = []
            const lines = logs.split("\n")
            tasksRunner.state.search_string.forEach(string => {
                reg = new RegExp(string)
                buff = lines.find(x => x.match(reg, 'g'))
                if (buff) {
                    output.push(buff)
                }
            })

            LOGworker.debug(` Job.logParser(logs) : Output is: ${output}`)
            resolve(output)
            return this
        })

    }

}

//*** MAIN function

function setWorkerOnPause(tasks) {
    let buff = []
    tasksRunner.setPausedState()
    tasks.forEach( task => {
        buff.push(task.task_id)
    })
    LOGworker.warn(`Tasks ids for resuming: ${buff}`)
}

function checkRunningTasks(tasks) {
    LOGworker.info('Checking running tasks if exist')
    producer.checkRunningTasks()
        .then( runningTasks => {
            if (runningTasks) {
                LOGworker.warn('RUNNING tasks found')
                if (tasks) {
                    LOGworker.warn('Worker on PAUSE. RUNNING jobs will be updated to PAUSE.')
                    runningTasks.forEach( task => {
                        tasksRunner.DBsetState(task, "PAUSED")
                    })
                } else {
                    LOGworker.warn('RUNNING jobs will be re-run immediately.')
                    rerunTasks(runningTasks)
                }
            }
        })
}

function rerunTasks(tasks) {
    let buff = []
    tasks.forEach( task => {
        buff.push(task.task_id)
        tasksRunner.init(task)
    })
    LOGworker.warn(`Tasks ids re-running: ${buff}`)
}

function main() {
    LOGworker.info('Checking paused tasks if exist')
    producer.checkPausedTasks()
        .then( tasks => {
            if (tasks) {
                LOGworker.info("Found tasks in PAUSE. Worker will set to PAUSED state immediately.")
                setWorkerOnPause(tasks)
            } else {
                LOGworker.info("There is no tasks in PAUSE")
            }
            return checkRunningTasks(tasks)
        })
        .catch( (err) => {
            LOGworker.error(` producer.getPausedTasksStatus() : Something went wrong.`)
            LOGworker.error(` producer.getPausedTasksStatus() : ${err}`)
        })


}

const tasksRunner = new TasksRunner(CONFIG)
main()

module.exports = {
    tasksRunner
}

//curl -X POST '0.0.0.0:8887/start' -H "Content-Type:application/json; charset=utf-8" -d '{"task_id": "TASK_ID_1", "jobs": [{"fullname": "bag/AppManage.2/aws_integration1b","params":{"Service":"bag-rs","bag service version":"latest","Action":"status"},"id": "1"},{"fullname": "account/AppManage.2/aws_integration1b","params":{"Service":"account","Account version":"latest","Action":"status"},"id": "2"},{"fullname": "address/AppManage.2/aws_integration1b","params":{"Service":"address","Address version":"latest","Action":"status"},"id": "3"}]}'
//curl -X POST '0.0.0.0:8887/start' -H "Content-Type:application/json; charset=utf-8" -d '{"task_id": "TASK_ID_2", "jobs": [{"fullname": "adjustment/AppManage.2/aws_integration1b","params":{"Service":"adjustment-rs","Adjustment service version":"latest","Action":"status"},"id": "1"},{"fullname": "authoring/AppManage.2/aws_integration1b","params":{"Service":"authoring-service","Authoring service version":"latest","Action":"status"},"id": "2"},{"fullname": "bestcoupon/AppManage.2/aws_integration1b","params":{"Service":"best-coupon-service","Best Coupon service version":"latest","Action":"status"},"id": "3"}]}'
//curl -X POST '0.0.0.0:8887/start' -H "Content-Type:application/json; charset=utf-8" -d '{"task_id": "TASK_ID_3", "jobs": [{"fullname": "bag/AppManage.2/aws_integration1b","params":{"Service":"bag-rs","bag service version":"latest","Action":"status"},"id": "1"},{"fullname": "bag/AppManage.2/aws_integration1b","params":{"Service":"bag-rs","bag service version":"latest","Action":"status"},"id": "2"},{"fullname": "bag/AppManage.2/aws_integration1b","params":{"Service":"bag-rs","bag service version":"latest","Action":"status"},"id": "3"}]}'
//curl -X POST '0.0.0.0:8887/start' -H "Content-Type:application/json; charset=utf-8" -d '{"task_id": "TASK_ID_1", "jobs": [{"fullname": "account/AppManage.2/aws_integration1b","params":{"Service":"account","Account version":"latest","Action":"status"},"id": "2"}]}'